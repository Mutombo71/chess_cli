class Piece:
    def __init__(self):
        self._pieces = {
            'p': u'\u265F',
            'r': u'\u265C',
            'n': u'\u265E',
            'b': u'\u265D',
            'k': u'\u265A',
            'q': u'\u265B',
            'P': u'\u2659',
            'R': u'\u2656',
            'N': u'\u2658',
            'B': u'\u2657',
            'K': u'\u2654',
            'Q': u'\u2655',
            '': u' ',
        }

    def piece(self, piece):
        return self._pieces[piece]


if __name__ == "__main__":
    piece = Piece()
    print(piece.piece('K'))
    print(piece.piece('k'))
