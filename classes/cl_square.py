from classes.cl_pieces import Piece


class Square:
    def __init__(self, color, piece):
        self._pc = Piece()
        self._color = color
        self._piece = self._pc.piece('')

    def color(self):
        return self._color

    def piece(self):
        return self._piece

    def set_piece(self, piece):
        self._piece = self._pc.piece(piece)
