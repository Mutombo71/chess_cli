from os import get_terminal_size
from classes.cl_square import Square


class Board:
    def __init__(self):
        self._board = []
        self._row = []
        self._fg = '1;38;2;0;0;0'
        self._bg_black = '48;2;118;150;86'
        self._bg_white = '48;2;186;202;68'
        self._coordinates = '48;2;128;128;128'
        self._size = get_terminal_size()
        self._columns = self._size.columns
        self._lines = self._size.lines
        self._empty_string = ' ' * (self._columns - 39)

        _color = False
        for i in range(0, 8):
            for row in range(0, 8):
                _square = Square(_color, '')
                self._row.append(_square)
                _color = not _color
            self._board.append(self._row)
            self._row = []
            _color = not _color

    # def print_field(self, row, column):
    #     print(self._board[row][column])

    def update(self, row, column, piece):
        square = self._board[row][column]
        square.set_piece(piece)

    def print_board(self):
        # print the complete Board
        print()
        print('   \N{ESC}['
              + self._coordinates
              + 'm     a  b  c  d  e  f  g  h     '
              + '\u001b[0m')
        print()
        line = ''
        for row in range(0, 8):
            for column in range(0, 8):
                square = self._board[row][column]
                if square.color():
                    if square.piece() != '':
                        square = '\N{ESC}[' \
                                + self._fg \
                                + ';' \
                                + self._bg_black \
                                + 'm ' \
                                + square.piece() \
                                + ' \u001b[0m'
                    else:
                        square = '\N{ESC}[' \
                                + self._fg \
                                + ';' \
                                + self._bg_black \
                                + 'm ' \
                                + '  \u001b[0m'
                else:
                    if square.piece() != '':
                        square = '\N{ESC}[' \
                                + self._fg \
                                + ';' \
                                + self._bg_white \
                                + 'm ' \
                                + square.piece() \
                                + ' \u001b[0m'
                    else:
                        square = '\N{ESC}[' \
                                + self._fg \
                                + ';' \
                                + self._bg_white \
                                + 'm ' \
                                + ' \u001b[0m'
                line = line + square
                rev_row = (8 - row)
                rev_row_formatted = ('%s' %
                                     ('\N{ESC}['
                                      + self._coordinates
                                      + 'm '
                                      + rev_row.__str__()
                                      + ' \u001b[0m'))
            print('   %s %s %s' %
                  (rev_row_formatted, line, rev_row_formatted))
            line = ''
        print()
        print('   \N{ESC}['
              + self._coordinates
              + 'm     a  b  c  d  e  f  g  h     '
              + '\u001b[0m')

    def clear_board(self) -> int:
        for i in range(0, 8):
            for j in range(0, 8):
                self.update(i, j, '')
        return 0

    def clear_info_area(self) -> int:
        for i in range(2, self._lines):
            print('\033[' + i.__str__()
                          + ';40H'
                          + self._empty_string)
        return 0

    def start_position(self):
        self.clear_board()

        # white side
        self.update(7, 0, 'R')
        self.update(7, 1, 'N')
        self.update(7, 2, 'B')
        self.update(7, 3, 'Q')
        self.update(7, 4, 'K')
        self.update(7, 5, 'B')
        self.update(7, 6, 'N')
        self.update(7, 7, 'R')
        for i in range(0, 8):
            self.update(6, i, 'P')

        # black side
        self.update(0, 0, 'r')
        self.update(0, 1, 'n')
        self.update(0, 2, 'b')
        self.update(0, 3, 'q')
        self.update(0, 4, 'k')
        self.update(0, 5, 'b')
        self.update(0, 6, 'n')
        self.update(0, 7, 'r')
        for i in range(0, 8):
            self.update(1, i, 'p')

    def fen2board(self, fen_position):

        self.clear_board()

        pos, player, castle, en_passant, half_moves, move_number =\
            fen_position.split(' ', 5)
        row = pos.split('/', 7)
        j = 0
        for i in range(0, 8):
            for char in row[i]:
                if char.isalpha():
                    self.update(i, j, char)
                    j += 1
                else:
                    j += int(char)
            j = 0


if __name__ == "__main__":
    board = Board()
    board.fen2board('rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR\
                     w KQkq - 0 1')
    board.print_board()
    board.fen2board('rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R\
                    b KQkq - 1 2')
    board.print_board()
