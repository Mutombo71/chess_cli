from os import system
from stockfish import Stockfish
from classes.cl_board import Board
from typing import List, Dict


class Game:

    def __init__(self):
        self._stockfish = Stockfish(path='/usr/bin/stockfish',
                                    depth=2,
                                    parameters={'Skill Level': 2})
        self._board = Board()
        self._white_moves: List[str] = []
        self._black_moves: List[str] = []
        self._white_fens: Dict[int, str] = {}
        self._black_fens: Dict[int, str] = {}
        self._erase = '\x1b[1A\x1b[2K'

        system('clear')
        self._board.start_position()
        self._board.print_board()

    def set_elo_rating(self, rating: int) -> int:
        self._stockfish.set_elo_rating(rating)
        return 0

    def print_moves(self) -> int:
        i: int = 2
        terminal_position: str = '\033[' + i.__str__() + ';40H'
        print(terminal_position + 'white | black')
        i += 1
        for white, black in zip(self._white_moves, self._black_moves):
            terminal_position: str = '\033[' + i.__str__() + ';40H'
            print(terminal_position + '%5s | %5s' % (white, black))
            i += 1
        return 0

    def print_fens(self) -> int:
        i: int = 2
        terminal_position: str = '\033[' + i.__str__() + ';40H'
        print(terminal_position + 'white: ')
        i += 1
        for round, position in self._white_fens.items():
            terminal_position: str = '\033[' + i.__str__() + ';40H'
            print(terminal_position + '%s: %s' % (round, position))
            i += 1
        terminal_position: str = '\033[' + i.__str__() + ';40H'
        print(terminal_position + 'black: ')
        i += 1
        for round, position in self._black_fens.items():
            terminal_position: str = '\033[' + i.__str__() + ';40H'
            print(terminal_position + '%s: %s' % (round, position))
            i += 1

        return 0

    def run(self) -> int:
        run = True
        command = ''
        round = 0

        while run:
            # input player command or move
            # move cursor to input position
            print('\033[28;0H')
            command = input('enter next move/cmd: ')
            # check if commmand contains a parameter value and
            # split it from command into extra variable in_value
            try:
                command, in_value = command.split(' ', 1)
            except ValueError:
                value = 0
            else:
                value = in_value

            match command:
                # end the game
                case 'quit':
                    run = False
                    continue

                # let stockfish give a hint
                case 'hint':
                    print(self._erase)

                    self._board.clear_info_area()

                    if (value == 0):
                        terminal_position: str = '\033[2;40H'
                        print(terminal_position + 'thinking 5 secs')
                        terminal_position: str = '\033[3;40H'
                        print(terminal_position + 'hint: %s' %
                              (self._stockfish.get_best_move_time(5000)))
                    else:
                        terminal_position: str = '\033[2;40H'
                        print(terminal_position
                              + 'thinking %2.2g secs'
                              % (int(value) / 1000.))
                        terminal_position: str = '\033[3;40H'
                        print(terminal_position + 'hint: %s' %
                              (self._stockfish.get_best_move_time(value)))
                    # return cursor to input position
                    print('\033[28;0H')
                    continue

                # list all moves
                case 'moves':
                    # print all moves taken so far
                    print(self._erase)

                    self._board.clear_info_area()

                    self.print_moves()

                    # return cursor to input position
                    print('\033[28;0H')
                    continue

                case 'fen':
                    print(self._erase)
                    self._board.clear_info_area()

                    # set board to a given fen position
                    # rnbqkbnr/1p2pppp/p2p4/2p5/2B1P3/5N2/PPPP1PPP/RNBQK2R w KQkq - 0 4
                    if (value == 0):
                        # return cursor to input position
                        print('\033[28;0H')
                        continue
                    else:
                        # set board view to new fen position
                        self._board.fen2board(value)

                        # reset fen positions
                        self._white_fens: Dict[int, str] = {}
                        self._black_fens: Dict[int, str] = {}

                        # set engine to new fen position
                        # and start new game
                        system('clear')
                        self._stockfish.set_fen_position(
                                value, True)
                        self._board.print_board()

                    # return cursor to input position
                    print('\033[28;0H')
                    continue

                case 'fens':
                    # print all previous board positions
                    print(self._erase)

                    self._board.clear_info_area()

                    self.print_fens()

                    # return cursor to input position
                    print('\033[28;0H')
                    continue

                case 'elo':
                    # print stockfish settings
                    print(self._erase)

                    self._board.clear_info_area()

                    i: int = 1
                    for param, value in self._stockfish\
                                            .get_parameters()\
                                            .items():
                        i += 1
                        terminal_position: str = '\033[' + i.__str__() + ';40H'
                        print(terminal_position + '%s: %s' % (param, value))

                    # return cursor to input position
                    print('\033[28;0H')
                    continue

                case 'redraw':
                    system('clear')
                    print(self._erase)

                    # refresh the board with the current position
                    self._board.fen2board(self._black_fens[round - 1])
                    self._board.print_board()

                    # return cursor to input position
                    print('\033[28;0H')
                    continue

                case 'eval':
                    print(self._erase)

                    board_evaluation = self._stockfish.get_evaluation()
                    print('%s: %s' % (board_evaluation['type'],
                                      board_evaluation['value']))
                    wdl_stats = self._stockfish.get_wdl_stats()
                    print('W | D | L : %s | %s | %s' % (wdl_stats[0],
                                                        wdl_stats[1],
                                                        wdl_stats[2]))
                    continue

                case 'back':
                    if (value == 0):
                        print('moving back one positon')
                        if (round < 2):
                            self._board.start_position()
                            round = 0
                        else:
                            # setting back to fen position
                            self._board.fen2board(self._black_fens[round - 2])
                            self._stockfish.set_fen_position(
                                    self._black_fens[round - 2], True)
                            round -= 1
                        system('clear')
                        self._black_moves = self._black_moves[:-1]
                        self._white_moves = self._white_moves[:-1]
                        del self._white_fens[round]
                        del self._black_fens[round]
                        self._board.print_board()

                    else:
                        value = int(value)
                        if (value > round - 2):
                            print('can\'t take back that many moves')
                        else:
                            print('moving back %i positions')
                            # setting back to fen position
                            self._board.fen2board(self._black_fens[round - (value + 1)])
                            self._stockfish.set_fen_position(
                                    self._black_fens[round - (value + 1)], True)
                            round -= value
                            system('clear')
                            # deleting positions from fen dict
                            self._black_moves = self._black_moves[:-value]
                            self._white_moves = self._white_moves[:-value]
                            for i in range(value):
                                del self._white_fens[round + i]
                                del self._black_fens[round + i]

                            self._board.print_board()

                    continue

                # if no command is entered we assume a 'normal' move
                # check if move is valid and make it
                case _:
                    if (self._stockfish.is_move_correct(command)):
                        self._stockfish.make_moves_from_current_position(
                                [command])
                        # add whites move to his move history
                        self._white_moves.append(command)
                        # get the current position in fen notation and add
                        # to list of fens
                        current_position = self._stockfish.get_fen_position()
                        self._white_fens[round] = current_position
                        # clear the screen for next board view
                        system('clear')
                        print('white: %s' % (self._white_moves[-3:]))
                        # refresh the board with the current position
                        self._board.fen2board(current_position)
                        self._board.print_board()

                    # if move is not valid, reinput
                    else:
                        print('wrong move/command entered')
                        continue

                    # opponents move
                    opp_move = self._stockfish.get_best_move_time(1000)
                    self._stockfish.make_moves_from_current_position(
                            [opp_move])
                    # add blacks to move to his move history
                    self._black_moves.append(opp_move)
                    current_position = self._stockfish.get_fen_position()
                    self._black_fens[round] = current_position
                    print('black: %s' % (self._black_moves[-3:]))
                    self._board.fen2board(current_position)
                    round += 1
                    self._board.print_board()

        return 0

    def quit(self) -> int:
        print('ending game')
        return 0


if __name__ == "__main__":
    game = Game()
