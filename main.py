#!/usr/bin/env python
from os import system
from classes.cl_game import Game


# IDEAS / TODOS
# connect to stockfish via tcp
# show what engine is thinking
# save game, maybe to database
# option to save game before taking back a move / fork a game
# make a start screen to set parameters
# set parameters for stockfish engine
# enable other chess engines -> create an abstract class
#   derive specific engine class from abstract engine class
# get top moves
# refresh display
# really make use of return codes
# mark the last move

# ## DONE
# set specific fen position, done 20220111
# print information in info pane (elo, what stockfish thinks, ...), done 20220111
# take back one move, done 20220107
# take back number of moves, done 20220109
# add wdl statistics, done 20220107
# add game evaluation, done 20220107
# rewrite command switching to case statement, done 20220107
# hint - add time to think/analyze, done 20220107

# QUESTIONS
# where shall I put settings / global variables?
# where do which type of return codes make sense?
# is coding like this ok?
#     self._white_fens.items()
#     self._white_moves.append(command)
# in the case statement is continue properly used?

def main() -> int:

    game = Game()

    game.set_elo_rating(800)
    game.run()

    game.quit()

    return 0


if __name__ == "__main__":
    system('clear')
    main()
